
# Approximate Bayesian Computation related methods

```@autodocs
Modules = [MarkovProcesses]
Pages   = ["algorithms/abc_smc.jl", "algorithms/abc_model_choice.jl"]
```

