
```@meta
CurrentModule = MarkovProcesses
```

# MarkovProcesses.jl

*A package for simulation, verification and parameter estimation of Chemical Reaction Networks.*

## Package features

* A core of simulation for Continuous-Time Markov Chains (CTMC) defined by Chemical Reaction Networks (CRN),
* A simple interface for Biochemical Networks / Stochastic Petri Nets,
* Synchronized simulation with Linear Hybrid Automata,
* Approximate Bayesian Computation, a likelihood-free inference method,
* Automaton-ABC: a statistical method for verification of parametric CTMCs.

